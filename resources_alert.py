import psutil
import datetime
import smtplib
import time
import socket

mail_list = ["resource.eyeqtech@gmail.com"]

def get_alert_msg():
    log = []
    host_name = socket.gethostname()
    time = datetime.datetime.now()
    time_str = str(time.day)+'/'+str(time.month)+'/'+str(time.year)+' '+str(time.hour)+':'+str(time.minute)+':'+str(time.second)

    log.append(host_name)
    log.append(time_str + '\n')
    log.append('CPU percent: ' + str(round(psutil.cpu_percent(interval=0.1, percpu=False),2)) + '%')
    log.append('RAM percent: ' + str(round(psutil.virtual_memory().percent,2)) + '%')
    log.append('Swap percent: ' + str(round(psutil.swap_memory().percent,2)) + '%')
    all_disk = psutil.disk_partitions(all=False)
    log.append('Available disk space:')
    for disk in all_disk:
        free_amount = psutil.disk_usage(disk.mountpoint).free/ (1024*1024*1024)
        log.append('  '+disk.mountpoint + ' ' + str(round(free_amount,2)) + ' GB' )
    return '\n'.join(log)

def init_SMTP_gmail(username,password):
    server = smtplib.SMTP('smtp.gmail.com',587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    return server

def main():
    while(True):
        server = init_SMTP_gmail("smtp.eyeqtech@gmail.com","@eyeqtech")
        print('Getting new Log')
        log = get_alert_msg()
        print(log)
        for mail_address in mail_list:
            msg = "\r\n".join([
            ("From: " + log[0]),
            ("To: " + mail_address),
            "Subject: Resource report",
            "",
            log
            ])
            server.sendmail("smtp.eyeqtech@gmail",mail_address , msg)
            print("Email sent to: " + mail_address)
        print('Go to sleep')
        server.quit()
        time.sleep(600)
    

main()